# Week 1 - Docker Client/Server Example (Step 6)

Use the Visual Studio Code Remote Containers extension as a development environment.

# Open Workspace in Container

Make sure to install the Microsoft "Remote Development" extension for VS Code.
This allows VS Code to open our working directory from within a container
similar to how we've been using a bind mount thus far.

This setup still depends upon a `docker-compose.yml` file to orchestrate
the docker workflow. However, you'll notice the lack of bind volume mount
and other simplifications.

To open the working directory in a remote container, click the green button
in the lower-left corner with the two-opposing arrows icon and select:

- `Remote-Containers: Reopen in Container`,
  from the command palette.

# Run Python Container

Just like in the previous step we should now be inside the Python container. Navigate to `python-client` and run `main.py`:

- `cd python-client`
- `source venv/bin/activate`
- `python main.py`


## Reference

https://code.visualstudio.com/docs/remote/containers
