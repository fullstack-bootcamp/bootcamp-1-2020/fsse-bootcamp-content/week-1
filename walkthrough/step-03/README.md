# Week 1 - Docker Client/Server Example (Step 3)

Update the Node.js docker image and run a container in the background.

## Node.js Docker Standalone Image

Make a new Node.js image by using the new `Dockerfile` and rebuilding:

```
docker build -t b2020-week1-node-standalone --file Dockerfile .
```

This image contains the server source code and does not need
to have our file system mounted.

## Run the Container in the Background

The Node.js server can be run in the background via a "detached"
container using our `b2020-week1-node-standalone` image:

```
docker run --detach \
--name node-server \
--publish 9000:9000 \
b2020-week1-node-standalone
```

The container should now be running. This can be confirmed via `docker ps`, which will provide an
output as shown below:

```
CONTAINER ID        IMAGE                         COMMAND                  CREATED
febeb27d5fbe        b2020-week1-node-standalone   "docker-entrypoint.s…"   5 seconds ago

...

STATUS              PORTS                    NAMES
Up 4 seconds        0.0.0.0:9000->9000/tcp   node-server
```

Again, this server can be checked via http://localhost:9000/route.

It's possible that you may see the following error: `Bind for 0.0.0.0:9000 failed: port is already allocated.`. If so, run the following commands to stop all running containers, prune the system, and attempt to run the container again:

- `docker stop $(docker ps -aq)`
- `docker system prune`

If you're on Windows and not using [Git Bash](https://gitforwindows.org/), you'll need to
write each container ID (e.g. `docker stop f9a2f116b97c`).
