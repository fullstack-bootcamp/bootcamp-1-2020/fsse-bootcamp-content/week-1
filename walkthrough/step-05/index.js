const express = require("express");
const app = express();
const PORT = 9000;

let timesRouteIsHitSinceReboot = 0;

app.get("/route", (req, res) => {
  console.log("Hit route /route");
  timesRouteIsHitSinceReboot = timesRouteIsHitSinceReboot + 1;
  //   timesRouteIsHitSinceReboot++

  res.json({ hello: "world", count: timesRouteIsHitSinceReboot });
});

app.listen(PORT);
