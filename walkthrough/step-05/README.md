# Week 1 - Docker Client/Server Example (Step 5)

Automate Docker workflow via Docker Compose.

## Docker Compose

The entire workflow of building images and running containers in a
particular order with various options can be automated via Docker Compose.
The configuration data used for automation is contained within the
file `docker-compose.yml`.

After this file is created, our Python container can be run interactively
via:

- `docker-compose run dev`

If you run into any `port is already allocated` errors remember to stop all currently
running containers and prune docker. Afterwards, try to run the `dev` container via
`docker-compose` again.

- `docker stop $(docker ps -aq)`
- `docker system prune`

# Run Python Client

We should now be inside the Python container. Navigate to `python-client` and
run `main.py`:

- `cd python-client`
- `source venv/bin/activate`
- `python main.py`
