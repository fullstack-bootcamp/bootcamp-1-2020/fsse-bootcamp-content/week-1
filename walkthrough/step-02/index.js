const express = require("express");
const app = express();
const PORT = 9000;

app.get("/route", (req, res) => {
  console.log("Hit route /route");
  res.json({ hello: "world" });
});

app.listen(PORT);
