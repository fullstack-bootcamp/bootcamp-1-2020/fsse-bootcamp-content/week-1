# Week 1 - Docker Client/Server Example (Step 2)

Install and run a Python HTTP client.

## Python Docker Environment

Similar to Node.js, we'll build another docker image for our Python client. Run
the following build command:

```
docker build -t b2020-week1-python --file Docker-python .
```

A container for this image can also be run interactively and mounted to the working directory:

```
docker run -it \
--mount type=bind,source="$(pwd)",target=/workspace \
b2020-week1-python:latest
```

- Note, if you're on Windows you'll need to replace `"$(pwd)"` with `%cd%`
  in the command above.

## Python HTTP Client

Within the docker container, you'll see a terminal prompt similar to `root@f6dc99a5e149:/workspace#`.

Next navigate into the `python-client` sub-directory and create a virtual
environment to install python dependencies:

1. `cd python-client`
1. `virtualenv venv`
1. `source venv/bin/activate`

You should now see a `(venv)` prefix in your terminal such as:

- `(venv) root@f6dc99a5e149:/workspace/python-client#`

Install the python dependencies and run `main.py` to send a `GET` request
to `http://google.com`:

1. `pip install -r requirements.txt`
1. `python main.py`
