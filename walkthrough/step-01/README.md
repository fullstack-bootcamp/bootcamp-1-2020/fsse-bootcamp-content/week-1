# Week 1 - Docker Client/Server Example (Step 1)

Install and run an HTTP server in the foreground with Node.js.

## Node.js Docker Environment

We can build a custom Node.js docker image by using a `Dockerfile` and executing the `build` command:

```
docker build -t b2020-week1-node .
```

Next an interactive container can be started with our working directory file system mounted inside.
We will develop a Node.js server using the container as our runtime environment:

```
docker run -it \
--publish 9000:9000 \
--mount type=bind,source="$(pwd)",target=/workspace \
b2020-week1-node:latest
```

- Note, if you're on Windows you'll need to replace `"$(pwd)"` with `%cd%`
  in the command above.

## Node.js Server

Within the docker container, you'll see a terminal prompt similar to `root@4f10c38d8449:/workspace#`.

In order to install and run the server execute the following commands:

1. `yarn install`
1. `node index.js`

Check that the server is running in your browser via http://localhost:9000/route.
You should see the following response body on the corresponding page:

```
{
   hello: "world"
}
```
