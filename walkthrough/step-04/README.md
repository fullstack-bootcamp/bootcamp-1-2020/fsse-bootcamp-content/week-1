# Week 1 - Docker Client/Server Example (Step 4)

Connect Python HTTP client to Node.js server in Docker.

## Rebuild Image

With the new Node.js server source code, rebuild its image:

```
docker build -t b2020-week1-node-standalone --file Dockerfile .
```

## Run and Link Containers

First run the Node.js server container in the background:

```
docker run --detach \
--name node-server \
--publish 9000:9000 \
b2020-week1-node-standalone
```

Next start the Python container in interactive mode and link it to the Node.js container:

```
docker run -it \
--mount type=bind,source="$(pwd)",target=/workspace \
--link node-server \
b2020-week1-python
```

## Send Request from Python to Node.js Container

From within the python container, navigate to `python-client` and activate the virtual environment:

- `cd python-client`
- `source venv/bin/activate`

Finally send a request via the Python client:

- `python main.py`

You should see the following response logged to the console:

```
{'hello': 'world', 'count': 1}
```

Note that from within the Python container we can refer to the Node.js container host
via its name `node-server` and port `9000`. The reason we can access it on `localhost` and
port `9000` in the browser is because its internal port is published to the
local machine's port `9000`. If the `--publish 9000:9000` argument were removed, we
would no longer be able to access the Node.js server directly from within the browser.
