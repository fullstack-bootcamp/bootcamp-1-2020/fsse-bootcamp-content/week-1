# Week 1 Lecture Code

This repository contains source code from the lecture on July 9, 2020.
Two directories are available:

## End result from the lecture

See contents in [lecture-20200709](https://gitlab.com/fullstack-bootcamp/bootcamp-1-2020/fsse-bootcamp-content/week-1/-/tree/master/lecture-20200709).

## Six step walkthrough

The [walkthrough](https://gitlab.com/fullstack-bootcamp/bootcamp-1-2020/fsse-bootcamp-content/week-1/-/tree/master/walkthrough)
will take you through the following steps:

1. Install and run an HTTP server in the foreground with Node.js
2. Install and run a Python HTTP client
3. Update the Node.js docker image and run a container in the background
4. Connect Python HTTP client to Node.js server in Docker
5. Automate Docker workflow via Docker Compose
6. Use the Visual Studio Code Remote Containers extension as a development environment

Each step includes its own `README.md` that will describe the necessary commands to get
everything running on your machine.
