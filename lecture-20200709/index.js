const express = require("express");

const app = express();
const PORT = 9000;

app.get("/route", (req, res) => {
  console.log("Hit route /route");
  res.json({ hello: "world" });
});

app.get("/my-name/:myName", (req, res) => {
  console.log(`${req.params.myName} says hello`);
  res.json({ hello: "world" });
});

app.listen(PORT);
